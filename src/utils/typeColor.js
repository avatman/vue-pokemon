const typeColor = (type) => {
    switch (type) {
        case "poison":
            return "#6d2b91";
        case "fire":
            return "#f50";
        case "flying":
            return "#d86f2d";
        case "water":
            return "#2db0d8";
        case "bug":
            return "#2d9146";
        case "electric":
            return "#f4f433";
        case "ground":
            return "#5b5b42";
        case "fairy":
            return "#c98fc4";
        case "grass":
            return "#9ee2b1";
        case "fighting":
            return "#c19f70";
        case "psychic":
            return "#7c82d6";
        case "steel":
            return "#bcbdc6";
        case "ice":
            return "blue";
        default:
            return "cyan";
    }
}

export default typeColor