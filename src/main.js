import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

Vue.config.productionTip = false

Vue.use(axios)
Vue.use(Antd)

axios.interceptors.request.use(function (config) {
  store.commit('load', true) // vuex mutation set loading state to true
  return config
}, function (error) {
  return Promise.reject(error)
})

axios.interceptors.response.use(function (config) {
  store.commit('load', false) // vuex mutation set loading state to false
  return config
}, function (error) {
  return Promise.reject(error)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
